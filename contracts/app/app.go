package saasapp

import (
	"gorm.io/gorm"

	"gitee.com/pangxianfei/multiapp/sysmodel"
)

type AppExample interface {
	//Initiation(ctx iris.Context) (AppDb *gorm.DB)
	SetTenantsDb(TenantsId int64, AppId int64) (AppDb *gorm.DB)
	SetSourceData(CurrInstanceDB *sysmodel.InstanceDb) (AppDb *gorm.DB)
	MysqlOpenDb(Instance *sysmodel.InstanceDb) (tenant *gorm.DB)
	OutConsole(title string, serial int64, AppId int64, DbName string)
}
