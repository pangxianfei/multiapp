package application

import "gitee.com/pangxianfei/multiapp/sysmodel"

type Application interface {
	GetById(id int64) *sysmodel.PlatformAdmin
	GetByMobile(mobile string) *sysmodel.PlatformAdmin
	GetByName(TenantName string) *sysmodel.PlatformAdmin
	GetAdminInfo(id int64) *sysmodel.PlatformAdmin
}
