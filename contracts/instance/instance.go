package instance

import (
	"gitee.com/pangxianfei/multiapp/requests"
	"gitee.com/pangxianfei/multiapp/sysmodel"
)

type Instance interface {
	GetAdminInfo(AdminId int64) *sysmodel.PlatformAdmin
	CreateUser(UserRegister requests.UserRegister) (Admin *sysmodel.PlatformAdmin, err error)
	CreateAppInstance(UserRegister requests.UserRegister, Admin *sysmodel.PlatformAdmin) (newInstanceDb []sysmodel.InstanceDb, err error)
	CreateDatabaseUserName(UserRegister requests.UserRegister, Admin *sysmodel.PlatformAdmin) error
	CreateDBuser(AdminId int64) error
	CreateDatabase(AdminId int64) ([]sysmodel.InstanceDb, error)
	//CreateLoginAccount(cxt iris.Context, UserName string, Mobile string, Password string) (*sysmodel.PlatformAdmin, error)
}
