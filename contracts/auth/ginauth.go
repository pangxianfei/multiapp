package auth

import (
	"gitee.com/pangxianfei/multiapp/request"
	"gitee.com/pangxianfei/multiapp/sysmodel"
)

type GinAuth interface {
	Guard(name string) GinAuth
	Parse(ctx request.Context) error
	User(ctx request.Context) *sysmodel.PlatformAdmin
	Login(ctx request.Context, Mobile string, Password string) (newAdmin *sysmodel.PlatformAdmin, token string, err error)
	LoginUsingID(ctx request.Context, userId int64) (newAdmin *sysmodel.PlatformAdmin, token string, err error)
	Refresh(ctx request.Context) (token string, err error)
	Logout(ctx request.Context) bool
}
