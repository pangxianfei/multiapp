package auth

type IrisAuth interface {
	Guard(name string) IrisAuth
	// Parse(ctx iris.Context) error
	// User(ctx iris.Context) *sysmodel.PlatformAdmin
	// Login(ctx iris.Context, UserLogin requests.UserLogin) (newAdmin *sysmodel.PlatformAdmin, token string, err error)
	// LoginUsingID(ctx iris.Context, userId int64) (newAdmin *sysmodel.PlatformAdmin, token string, err error)
	// Refresh(ctx iris.Context) (token string, err error)
	// Logout(ctx iris.Context) bool
}
