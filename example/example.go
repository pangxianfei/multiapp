package example

import (
	"database/sql"
	"fmt"

	"gitee.com/pangxianfei/multiapp/request"
	"gitee.com/pangxianfei/multiapp/simple"
	"gitee.com/pangxianfei/multiapp/sysmodel"
	"gorm.io/gorm"

	"gitee.com/pangxianfei/multiapp/config"
	"gitee.com/pangxianfei/multiapp/console"
	"gitee.com/pangxianfei/multiapp/consts"
	"gitee.com/pangxianfei/multiapp/database"

	"gitee.com/pangxianfei/multiapp/buffer"
	"gitee.com/pangxianfei/multiapp/global"
)

type Example struct{}

// Initiation iris框架专用
// func (app *Example) Initiation(ctx iris.Context) (AppExampleDB *gorm.DB) {
// 	AppId := ctx.Values().Get("AppId").(int64)
// 	return app.SetAppExampleDB(AppId)
// }

// InitAppDb gin框架专用
func (app *Example) InitAppDb(ctx request.Context) (AppExampleDB *gorm.DB) {
	AppId := ctx.GetInt64("AppId")
	return app.SetAppExampleDB(AppId)
}

func (app *Example) SetAppExampleDB(AppId int64) (AppDB *gorm.DB) {
	if AppId <= 0 {
		return nil
	}
	var CurrInstanceDB *sysmodel.InstanceDb

	//缓存编号
	var AppExampleDBDBNo string
	app.OutConsole("缓存编号", AppId, AppId, AppExampleDBDBNo)

	AppExampleDBDBNo = fmt.Sprintf(consts.APP_DB_KEY, AppId)
	//获取db缓存对象
	newAppIdAppDB := buffer.CacheInstanceDb.GetCacheInstanceDb(AppId)
	if newAppIdAppDB != nil && newAppIdAppDB.ID > 0 && newAppIdAppDB.AppId == AppId {
		CurrInstanceDB = newAppIdAppDB
	} else {
		tenantDbWhere := &sysmodel.RetrieveDB{AppId: AppId}
		if err := simple.DB().Debug().Where(tenantDbWhere).Find(&CurrInstanceDB).Error; err != nil {
			return nil
		}
		//设置db缓存对象
		buffer.CacheInstanceDb.SetCacheInstanceDb(CurrInstanceDB)
	}

	if _, ok := global.InstanceDBMapsObject[AppExampleDBDBNo]; ok && global.InstanceDBMapsObject[AppExampleDBDBNo].Db != nil && config.GetBool("database.db_object_cache") {
		var ThemTenantsDB *sysmodel.InstanceObjectDB
		ThemTenantsDB = global.InstanceDBMapsObject[AppExampleDBDBNo]
		if ThemTenantsDB.AppId == AppId && CurrInstanceDB.AppId == AppId {
			AppDB = global.InstanceDBMapsObject[AppExampleDBDBNo].Db
			app.OutConsole("缓存", ThemTenantsDB.AppId, ThemTenantsDB.AppId, ThemTenantsDB.DbName)
		}
	}

	if AppDB != nil {
		return AppDB.Session(&gorm.Session{})
	}

	app.OutConsole("全新连接", AppId, CurrInstanceDB.AppId, CurrInstanceDB.DbName)
	return app.SetSourceData(CurrInstanceDB)
}

func (app *Example) SetSourceData(CurrInstanceDB *sysmodel.InstanceDb) (InstanceDB *gorm.DB) {
	var mysqlStr string
	var dber *sql.DB
	AppExampleDBDBNo := fmt.Sprintf(consts.APP_DB_KEY, CurrInstanceDB.AppId)
	InstanceDB = app.MysqlOpenDb(CurrInstanceDB)

	SaveConnectionObject := sysmodel.InstanceObjectDB{}
	SaveConnectionObject.DbName = CurrInstanceDB.DbName
	SaveConnectionObject.AppName = CurrInstanceDB.AppName
	SaveConnectionObject.AppId = CurrInstanceDB.AppId
	SaveConnectionObject.Prefix = CurrInstanceDB.Prefix
	SaveConnectionObject.DriverName = CurrInstanceDB.DriverName
	SaveConnectionObject.Db = InstanceDB
	SaveConnectionObject.Dber = dber
	SaveConnectionObject.ConnStr = mysqlStr
	global.InstanceDBMutex.Lock()
	defer global.InstanceDBMutex.Unlock()
	global.InstanceDBMapsObject[AppExampleDBDBNo] = &SaveConnectionObject
	return InstanceDB
}

func (app *Example) MysqlOpenDb(Instance *sysmodel.InstanceDb) (AppDB *gorm.DB) {
	var DbDns string
	//如果是使用外网IP
	if Instance.NetSwitch == 2 {
		DbDns = Instance.OuterNet
	} else {
		//内网IP
		DbDns = Instance.Host
	}
	_, AppDB = database.OpenDB(Instance.DriverName, DbDns, Instance.Dbuser, Instance.Password, Instance.DbName, Instance.Port, Instance.Prefix, Instance.Charset, Instance.SetmaxIdleconns, Instance.Setmaxopenconns, Instance.Setconnmaxlifetime)
	return AppDB
}

func (app *Example) OutConsole(title string, serial int64, AppId int64, DbName string) {
	var consoleSTR string
	consoleSTR = " " + console.Sprintf(console.CODE_WARNING, "序号%d:连接类型:%s - 应用ID:%d  - 数据库:%s", serial, title, AppId, DbName)
	if config.Instance.TenantDb {
		console.Println(console.CODE_WARNING, consoleSTR)
	}
}
