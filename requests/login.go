package requests

// UserLogin 登陆验证器
type UserLogin struct {
	Mobile   string `json:"mobile" validate:"required,len=11"`
	Password string `json:"password" validate:"required,min=7,max=24"`
}
