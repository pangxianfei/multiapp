package requests

type AppHeaderAuthorization struct {
	Authorization string `header:"Authorization,required,min=20"`
}
type AppGinHeaderAuthorization struct {
	Authorization string `header:"Authorization" binding:"required,min=20"`
}
