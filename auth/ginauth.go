package auth

import (
	"gitee.com/pangxianfei/multiapp/request"

	contractAuth "gitee.com/pangxianfei/multiapp/contracts/auth"
	"gitee.com/pangxianfei/multiapp/sysmodel"
)

type GinAuth struct{}

func (a *GinAuth) Guard(name string) contractAuth.GinAuth {
	//TODO implement me
	panic("implement me")
}

func (a *GinAuth) Parse(ctx request.Context) error {
	//TODO implement me
	panic("implement me")
}

func (a *GinAuth) User(ctx request.Context) *sysmodel.PlatformAdmin {
	//return services.LoginService.User(ctx)
	panic("implement me")
}

func (a *GinAuth) Login(ctx request.Context, Mobile string, Password string) (newAdmin *sysmodel.PlatformAdmin, token string, err error) {
	//return services.LoginService.Login(ctx, Mobile, Password)
	panic("implement me")
}

func (a *GinAuth) LoginUsingID(ctx request.Context, userId int64) (newAdmin *sysmodel.PlatformAdmin, token string, err error) {
	//TODO implement me
	panic("implement me")
}

func (a *GinAuth) Refresh(ctx request.Context) (token string, err error) {
	//TODO implement me
	panic("implement me")
}

func (a *GinAuth) Logout(ctx request.Context) bool {
	//TODO implement me
	panic("implement me")
}
