package cache

import (
	"gitee.com/pangxianfei/multiapp/cache/driver"
)

type Cacher interface {
	driver.ProtoCache
	driver.BasicCache
}
