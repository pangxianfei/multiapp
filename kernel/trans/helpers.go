package trans

import (
	"gitee.com/pangxianfei/multiapp/config"
	"gopkg.in/go-playground/validator.v9"

	"gitee.com/pangxianfei/multiapp/resources/lang"

	"gitee.com/pangxianfei/multiapp/resources/lang/helper"
)

func ValidationTranslate(v *validator.Validate, langName string, e validator.ValidationErrors) lang.ValidationError {
	return helper.ValidationTranslate(v, langName, e)
}
func CustomTranslate(messageID string, data map[string]interface{}) string {
	return helper.CustomTranslate(messageID, data, config.GetString("app.locale"))
}
