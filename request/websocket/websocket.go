package websocket

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"

	"gitee.com/pangxianfei/multiapp/facades"
	"gitee.com/pangxianfei/multiapp/kernel/zone"
	requesthttp "gitee.com/pangxianfei/multiapp/request/http"
	"gitee.com/pangxianfei/multiapp/tmaic"
)

func ConvertHandler(wsHandler Handler) gin.HandlerFunc {
	var wsUpgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
	var pingPeriod = (wsHandler.ReadTimeout() * 9) / 10
	return func(c *gin.Context) {
		tmaicContext := requesthttp.ConvertContext(c)

		// create connectionHub
		hub := newConnectionHub(tmaicContext, wsHandler)

		ws, err := wsUpgrader.Upgrade(tmaicContext.Writer(), tmaicContext.Request(), nil)
		if err != nil {

			facades.Logger.Error("Failed to set websocket upgrade :" + err.Error())
			tmaicContext.JSON(http.StatusUnprocessableEntity, tmaic.V{"error": err})
			return
		}
		// close ws connection
		defer func() {
			if err := ws.Close(); err != nil {
				facades.Logger.Error(err.Error())
			}
			hub.close()
		}()

		// handle panic
		defer func() {
			if _err := recover(); _err != nil {
				if __err, ok := _err.(error); ok {
					facades.Logger.Error(__err.Error())
					return
				}
				facades.Logger.Error(_err.(error).Error())
				return
			}
		}()

		// setMaxSize
		ws.SetReadLimit(wsHandler.MaxMessageSize())

		// set handlers
		setPingPongCloseHandlers(ws, wsHandler, hub)

		// --------------------------
		// ---- websocket process ----
		// --------------------------

		// send msg
		go func() {
			pingTicker := zone.NewTicker(pingPeriod)
			defer pingTicker.Stop()

			for {
				if err := wsHandler.Loop(hub); err != nil {
					return
				}

				select {
				// send message
				case msg, ok := <-hub.getChan():
					if !ok {
						return
					}
					if msg.isDone() {
						return
					}
					if err := msg.send(ws, wsHandler); err != nil {
						facades.Logger.Error(err.Error())
						return
					}

				// send ping
				case <-pingTicker.C:
					ping := Msg{
						msgType: websocket.PingMessage,
						data:    &[]byte{},
						err:     nil,
					}
					if err := ping.send(ws, wsHandler); err != nil {
						facades.Logger.Error(err.Error())
						return
					}

				default:
					continue
				}
			}
		}()

		// read msg
		for {
			msg := &Msg{}
			if msg.scan(ws, wsHandler) != nil {
				facades.Logger.Error(err.Error())
				return
			}

			switch msg.msgType {
			// TextMessage denotes a text data message. The text message payload is
			// interpreted as UTF-8 encoded text data.
			case websocket.TextMessage:
				fallthrough
			// BinaryMessage denotes a binary data message.
			case websocket.BinaryMessage:
				wsHandler.OnMessage(hub, msg)
			// PingMessage denotes a ping control message. The optional message payload
			// is UTF-8 encoded text.
			case websocket.PingMessage: // defined at SetPingHandler
				// send pong
				pong := Msg{
					msgType: websocket.PongMessage,
					data:    &[]byte{},
					err:     nil,
				}
				if err := pong.send(ws, wsHandler); err != nil {
					facades.Logger.Error(err.Error())
					return
				}
			// PongMessage denotes a pong control message. The optional message payload
			// is UTF-8 encoded text.
			case websocket.PongMessage: // defined at SetPongHandler
				// check deadline
				if msg.err != nil {
					facades.Logger.Error(err.Error())
					return
				}
			case websocket.CloseMessage: // defined at SetCloseHandler
				closeMsg := Msg{
					msgType: websocket.CloseMessage,
					data:    &[]byte{},
					err:     nil,
				}
				if err := closeMsg.send(ws, wsHandler); err != nil {
					facades.Logger.Error(err.Error())
					return
				}

				return
			default:
				facades.Logger.Warn("No websocket handler on this msgType:" + fmt.Sprintf("%d", msg.msgType))
			}

		}
	}
}

func setPingPongCloseHandlers(ws *websocket.Conn, wsHandler Handler, hub Hub) {
	ws.SetCloseHandler(func(code int, text string) error {
		wsHandler.OnClose(hub, code, text)
		return nil
	})
	ws.SetPingHandler(func(appData string) error {
		wsHandler.OnPing(hub, appData)
		return nil
	})
	ws.SetPongHandler(func(appData string) error {
		wsHandler.OnPing(hub, appData)
		return nil
	})
}
