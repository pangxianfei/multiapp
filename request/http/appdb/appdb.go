package appdb

import (
	"gorm.io/gorm"
)

type DbManager interface {
	AppDb(AppId int64) *gorm.DB
	ContextDb() *gorm.DB
}
