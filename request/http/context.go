package http

import (
	"net/http"
	"reflect"

	"gitee.com/pangxianfei/multiapp/global"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	"gitee.com/pangxianfei/multiapp/request/http/auth"

	"gitee.com/pangxianfei/multiapp/config"

	"gitee.com/pangxianfei/multiapp/utils/jwt"
)

const ContextClaimKey = "TMAIC_CONTEXT_CLAIM"
const ContextIuserModelKey = "TMAIC_CONTEXT_IUSER_MODEL"

type ContextManager struct {
	*gin.Context
	*auth.RequestUser
}

func (c *ContextManager) AppDb(AppId int64) *gorm.DB {
	return global.SetAppExampleDB(AppId)
}

func (c *ContextManager) ContextDb() *gorm.DB {
	AppId := c.GetInt64("AppId")
	if AppId > 0 {
		return c.AppDb(AppId)
	}
	return nil
}

func (c *ContextManager) GinContext() *gin.Context {
	return c.Context
}
func (c *ContextManager) Request() *http.Request {
	return c.Context.Request
}
func (c *ContextManager) Writer() gin.ResponseWriter {
	return c.Context.Writer
}
func (c *ContextManager) SetRequest(r *http.Request) {
	c.Context.Request = r
}
func (c *ContextManager) SetWriter(w gin.ResponseWriter) {
	c.Context.Writer = w
}

func (c *ContextManager) Params() gin.Params {
	return c.Context.Params
}
func (c *ContextManager) Accepted() []string {
	return c.Context.Accepted
}
func (c *ContextManager) Keys() map[string]interface{} {
	return c.Context.Keys
}
func (c *ContextManager) Errors() []*gin.Error {
	return c.Context.Errors
}
func (c *ContextManager) SetAuthClaim(claims *jwt.UserClaims) {
	c.Set(ContextClaimKey, claims)
}
func (c *ContextManager) AuthClaimID() (ID int64, exist bool) {
	claims, exist := c.Get(ContextClaimKey)
	if !exist {
		return 0, false
	}
	//r, _ := utf8.DecodeRune([]byte(claims.(*jwt.UserClaims).ID))

	return int64(claims.(*jwt.UserClaims).ID), true
}
func (c *ContextManager) SetIUserModel(iuser auth.IUser) {
	c.Set(ContextIuserModelKey, iuser)
}
func (c *ContextManager) IUserModel() auth.IUser {
	iuser, exist := c.Get(ContextIuserModelKey)

	var typeof reflect.Type
	if !exist {

		typeof = reflect.TypeOf(config.GetInterface("auth.model_ptr"))
	} else {

		typeof = reflect.TypeOf(iuser.(auth.IUser))
	}

	ptr := reflect.New(typeof).Elem()
	val := reflect.New(typeof.Elem())
	ptr.Set(val)
	return ptr.Interface().(auth.IUser)
}

func (c *ContextManager) ScanUserWithJSON() (isAbort bool) {
	if err := c.ScanUser(); err != nil {
		return true
	}
	return false
}
func ConvertContext(c *gin.Context) *ContextManager {
	_c := &ContextManager{Context: c, RequestUser: &auth.RequestUser{}}

	_c.RequestUser.SetContext(_c)

	return _c
}
