package request

import (
	"github.com/gin-gonic/gin"

	"gitee.com/pangxianfei/multiapp/context"
	"gitee.com/pangxianfei/multiapp/request/http/appdb"
	"gitee.com/pangxianfei/multiapp/request/http/auth"

	"gitee.com/pangxianfei/multiapp/utils/jwt"
)

type Context interface {
	context.HttpContextor
	GinContext() *gin.Context
	SetAuthClaim(claims *jwt.UserClaims)
	SetIUserModel(iUser auth.IUser)
	auth.Context
	auth.RequestIUser
	appdb.DbManager
}
