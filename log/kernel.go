package log

import "gitee.com/pangxianfei/multiapp/facades"

func Initialize() {
	facades.Log = NewLogrusApplication()
}
