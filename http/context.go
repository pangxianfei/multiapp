package http

import (
	"net/http"
	"reflect"

	"github.com/gin-gonic/gin"

	"gitee.com/pangxianfei/multiapp/request/http/auth"

	"gitee.com/pangxianfei/multiapp/config"

	"gitee.com/pangxianfei/multiapp/utils/jwt"
)

const ContextClaimKey = "TMAIC_CONTEXT_CLAIM"
const ContextIuserModelKey = "TMAIC_CONTEXT_IUSER_MODEL"

type httpContext struct {
	*gin.Context
	*auth.RequestUser
}

func (c *httpContext) GinContext() *gin.Context {
	return c.Context
}
func (c *httpContext) Request() *http.Request {
	return c.Context.Request
}
func (c *httpContext) Writer() gin.ResponseWriter {
	return c.Context.Writer
}
func (c *httpContext) SetRequest(r *http.Request) {
	c.Context.Request = r
}
func (c *httpContext) SetWriter(w gin.ResponseWriter) {
	c.Context.Writer = w
}

func (c *httpContext) Params() gin.Params {
	return c.Context.Params
}
func (c *httpContext) Accepted() []string {
	return c.Context.Accepted
}
func (c *httpContext) Keys() map[string]interface{} {
	return c.Context.Keys
}
func (c *httpContext) Errors() []*gin.Error {
	return c.Context.Errors
}
func (c *httpContext) SetAuthClaim(claims *jwt.UserClaims) {
	c.Set(ContextClaimKey, claims)
}
func (c *httpContext) AuthClaimID() (ID int64, exist bool) {
	claims, exist := c.Get(ContextClaimKey)
	if !exist {
		return 0, false
	}
	return int64(claims.(*jwt.UserClaims).ID), true
}
func (c *httpContext) SetIUserModel(iuser auth.IUser) {
	c.Set(ContextIuserModelKey, iuser)
}
func (c *httpContext) IUserModel() auth.IUser {
	iuser, exist := c.Get(ContextIuserModelKey)

	var typeof reflect.Type
	if !exist {
		// default use config IUser model
		typeof = reflect.TypeOf(config.GetInterface("auth.model_ptr"))
	} else {
		// or use user set by middleware.IUser
		typeof = reflect.TypeOf(iuser.(auth.IUser))
	}

	ptr := reflect.New(typeof).Elem()
	val := reflect.New(typeof.Elem())
	ptr.Set(val)
	return ptr.Interface().(auth.IUser)
}

func (c *httpContext) ScanUserWithJSON() (isAbort bool) {
	if err := c.ScanUser(); err != nil {

		return true
	}
	return false
}
func ConvertContext(c *gin.Context) *httpContext {
	_c := &httpContext{Context: c, RequestUser: &auth.RequestUser{}}

	_c.RequestUser.SetContext(_c)

	return _c
}
