package middleware

import (
	"sync"
)

var once sync.Once

// 所有中间件都要添加在这里注册，注册后才能使用
func init() {
	once.Do(func() {
		//系统中间件
		initCORS()
	})
}
