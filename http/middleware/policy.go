package middleware

import (
	"gitee.com/pangxianfei/multiapp/policy"

	"gitee.com/pangxianfei/multiapp/request"
)

func Policy(_policy policy.Policier, action policy.Action) request.HandlerFunc {
	return func(c request.Context) {
		policy.Middleware(_policy, action, c, c.Params())
	}
}
