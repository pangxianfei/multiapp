package middleware

import (
	"github.com/gin-gonic/gin"

	"gitee.com/pangxianfei/multiapp/request"
)

func Logger() request.HandlerFunc {
	return func(c request.Context) {
		gin.Logger()(c.GinContext())
	}
}
