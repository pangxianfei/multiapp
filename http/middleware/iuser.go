package middleware

import (
	"gitee.com/pangxianfei/multiapp/request"

	"gitee.com/pangxianfei/multiapp/request/http/auth"
)

func IUser(userModelPtr auth.IUser) request.HandlerFunc {
	return func(c request.Context) {
		c.SetIUserModel(userModelPtr)

		c.Next()
	}
}
