package controller

import (
	"gitee.com/pangxianfei/multiapp/response"
)

func ReturnJson(httpCode int, message string, data interface{}) *response.JsonResult {
	return &response.JsonResult{
		Code:    httpCode,
		Message: message,
		Data:    data,
		Success: httpCode,
	}
}
