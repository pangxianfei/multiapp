package Controller

import (
	"gitee.com/pangxianfei/multiapp/policy"
	"gitee.com/pangxianfei/multiapp/request/http/auth"
	"gitee.com/pangxianfei/multiapp/validator"
)

type BaseGinController struct {
	validator.Validation
	policy.Authorization
	auth.RequestUser
}
