package Controller

import (
	"gitee.com/pangxianfei/multiapp/policy"

	"gitee.com/pangxianfei/multiapp/request/http/auth"

	"gitee.com/pangxianfei/multiapp/validator"
)

type Controller interface {
	Validate(c validator.Context, _validator interface{}, onlyFirstError bool) (isAbort bool)

	Authorize(c policy.Context, policies policy.Policier, action policy.Action) (permit bool, user auth.IUser)
}
