package Controller

import (
	"gitee.com/pangxianfei/multiapp/policy"
	"gitee.com/pangxianfei/multiapp/request/http/auth"

	"gitee.com/pangxianfei/multiapp/validator"
)

type BaseController struct {
	policy.Authorization
	auth.RequestUser
	validator.Validation
}
