package middleware

import (
	"gitee.com/pangxianfei/multiapp/request"

	"gitee.com/pangxianfei/multiapp/example"
)

// AppMiddleware 未使用
func AppMiddleware(ctx request.Context, AppId int64) error {
	ctx.Set("AppId", AppId)
	new(example.Example).SetAppExampleDB(AppId)
	return nil
}
