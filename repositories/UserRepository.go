package repositories

import (
	"gitee.com/pangxianfei/multiapp/sysmodel"
	"gorm.io/gorm"
)

var UserRepository = new(userRepository)

type userRepository struct {
}

func (r *userRepository) Take(db *gorm.DB, where ...interface{}) *sysmodel.PlatformAdmin {
	ret := &sysmodel.PlatformAdmin{}
	if err := db.Debug().Take(ret, where...).Error; err != nil {
		return nil
	}
	return ret
}

func (r *userRepository) GetByMobile(db *gorm.DB, mobile string) *sysmodel.PlatformAdmin {
	return r.Take(db, "mobile = ?", mobile)
}

func (r *userRepository) GetById(db *gorm.DB, id int64) *sysmodel.PlatformAdmin {
	return r.Take(db, "id = ?", id)
}
