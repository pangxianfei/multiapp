package repositories

import (
	"gitee.com/pangxianfei/multiapp/sysmodel"
	"gorm.io/gorm"
)

var AdminRepository = new(adminRepository)

type adminRepository struct {
}

func (r *adminRepository) Take(db *gorm.DB, where ...interface{}) *sysmodel.PlatformAdmin {
	ret := &sysmodel.PlatformAdmin{}
	if err := db.Debug().Take(ret, where...).Error; err != nil {
		return nil
	}
	return ret
}

func (r *adminRepository) GetById(db *gorm.DB, AdminId int64) *sysmodel.PlatformAdmin {
	return r.Take(db, "id = ?", AdminId)
}

func (r *adminRepository) GetByMobile(db *gorm.DB, mobile string) *sysmodel.PlatformAdmin {
	return r.Take(db, "mobile = ?", mobile)
}

func (r *adminRepository) Create(db *gorm.DB, admin *sysmodel.PlatformAdmin) (err error) {
	err = db.Create(admin).Error
	return
}

func (r *adminRepository) UserRegister(db *gorm.DB, admin *sysmodel.PlatformAdmin) (*sysmodel.PlatformAdmin, error) {

	TenantUser := &sysmodel.PlatformAdmin{
		Mobile:          admin.Mobile,
		UserName:        admin.UserName,
		Email:           admin.Email,
		EmailVerified:   admin.EmailVerified,
		Nickname:        admin.Nickname,
		Avatar:          admin.Avatar,
		BackgroundImage: admin.BackgroundImage,
		//Password:         admin.Password,
		HomePage:         admin.HomePage,
		Description:      admin.Description,
		Score:            admin.Score,
		Status:           admin.Status,
		TopicCount:       admin.TopicCount,
		CommentCount:     admin.CommentCount,
		Roles:            admin.Roles,
		UserType:         admin.UserType,
		ForbiddenEndTime: admin.ForbiddenEndTime,
		CreateTime:       admin.CreateTime,
		UpdateTime:       admin.UpdateTime,
	}
	err := db.Create(TenantUser).Error
	return TenantUser, err
}
