package repositories

import (
	"errors"

	"gitee.com/pangxianfei/multiapp/sysmodel"
	"gorm.io/gorm"
)

var RolesRepository = new(RolesDao)

type RolesDao struct {
}

// Delete 主键条件删除
func (r *RolesDao) Delete(db *gorm.DB, id int64) error {
	return db.Delete(&sysmodel.SysRoles{}, "id = ?", id).Error
}

func (r *RolesDao) Take(db *gorm.DB, where ...interface{}) *sysmodel.SysRoles {
	ret := &sysmodel.SysRoles{}
	if err := db.Debug().Take(ret, where...).Error; err != nil {
		return nil
	}
	return ret
}

func (r *RolesDao) GetByName(db *gorm.DB, name string) *sysmodel.SysRoles {
	return r.Take(db, "name = ?", name)
}

func (r *RolesDao) GetById(db *gorm.DB, id int64) *sysmodel.SysRoles {
	return r.Take(db, "id = ?", id)
}
func (r *RolesDao) Create(db *gorm.DB, roles *sysmodel.SysRoles) (*sysmodel.SysRoles, error) {
	//表不存则创建表
	r.IsHasTable(db)
	if r.GetByName(db, roles.Name) != nil {
		return nil, errors.New("角色已存在")
	}
	err := db.Create(roles).Error
	return roles, err
}
func (r *RolesDao) IsHasTable(db *gorm.DB) {
	if db.Migrator().HasTable(&sysmodel.SysRoles{}) == false {
		db.Migrator().CreateTable(&sysmodel.SysRoles{})
	}
}

func (r *RolesDao) AnyRole(db *gorm.DB, roleId int64) (rolesList []sysmodel.SysRoles) {
	var SysRoles = &sysmodel.SysRoles{Id: roleId}
	db.Model(SysRoles).Where(SysRoles).Preload("Permissions").Find(&rolesList)
	return rolesList
}

func (r *RolesDao) RoleInfo(db *gorm.DB, roleId int64) (roles sysmodel.SysRoles) {
	var SysRoles = &sysmodel.SysRoles{Id: roleId}
	db.Model(SysRoles).Where(SysRoles).Preload("Permissions").First(&roles)
	return
}

func (r *RolesDao) RemoveRole(db *gorm.DB, roleId int64) error {
	if err := r.Delete(db, roleId); err == nil {
		roleErr := RolePermissionRepository.DeleteRolePermission(db, roleId)
		if roleErr == nil {
			return roleErr
		}
		return err
	}
	return nil
}
