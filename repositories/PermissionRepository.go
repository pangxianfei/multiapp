package repositories

import (
	"gitee.com/pangxianfei/multiapp/simple"
	"gitee.com/pangxianfei/multiapp/simple/sqlcmd"
	"gitee.com/pangxianfei/multiapp/sysmodel"
	"gorm.io/gorm"
)

var PermissionRepository = new(synPermissionDao)

type synPermissionDao struct {
}

// DeleteRolePermission 角色ID条件删除
func (r *synPermissionDao) DeleteRolePermission(db *gorm.DB, roleId int64) error {
	return db.Where("role_id = ?", roleId).Delete(&sysmodel.RolePermissions{}).Error
}
func (r *synPermissionDao) Take(db *gorm.DB, where ...interface{}) *sysmodel.Permissions {
	ret := &sysmodel.Permissions{}
	if err := db.Take(ret, where...).Error; err != nil {
		return nil
	}
	return ret
}

// Find 返回列表
func (r *synPermissionDao) Find(db *gorm.DB, cnd *sqlcmd.Cnd) (list []sysmodel.Permissions) {
	cnd.Find(db, &list)
	return
}

func (r *synPermissionDao) GetByMobile(mobile string) *sysmodel.Permissions {
	return r.Take(simple.DB(), "mobile = ?", mobile)
}

func (r *synPermissionDao) GetByTenantName(TenantName string) *sysmodel.Permissions {
	return r.Take(simple.DB(), "tenant_name = ?", TenantName)
}

func (r *synPermissionDao) Create(db *gorm.DB, SysPermissions *sysmodel.Permissions) (Tenants *sysmodel.Permissions, err error) {
	r.IsHasTable(db)
	if err := db.Transaction(func(tx *gorm.DB) error {
		if err := tx.FirstOrCreate(SysPermissions, sysmodel.Permissions{PermissionId: SysPermissions.PermissionId}).Error; err != nil {
			return err
		}
		return nil
	}); err != nil {
		return nil, err
	}
	return SysPermissions, err
}

func (r *synPermissionDao) HasPermission(db *gorm.DB, userId int64, routeName string) *sysmodel.AdminPermissions {
	Permissions := &sysmodel.AdminPermissions{UserId: userId, RouteName: routeName}
	PermissionsRows := db.Where(Permissions).First(&Permissions)
	if PermissionsRows.Error != nil {
		return nil
	}
	if PermissionsRows.RowsAffected > 0 {
		return Permissions
	}
	return nil

}

func (r *synPermissionDao) IsHasTable(db *gorm.DB) {
	if db.Migrator().HasTable(&sysmodel.Permissions{}) == false {
		db.Migrator().CreateTable(&sysmodel.Permissions{})
	}
}
