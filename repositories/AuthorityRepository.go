package repositories

import (
	"gitee.com/pangxianfei/multiapp/sysmodel"
	"gorm.io/gorm"
)

var AuthorityRepository = new(authorityDao)

type authorityDao struct {
}

func (r *authorityDao) Take(db *gorm.DB, where ...interface{}) *sysmodel.Authority {
	ret := &sysmodel.Authority{}
	if err := db.Debug().Take(ret, where...).Error; err != nil {
		return nil
	}
	return ret
}

func (r *authorityDao) GetByName(db *gorm.DB, name string) *sysmodel.Authority {
	return r.Take(db, "name = ?", name)
}

// app权限列表
func (r *authorityDao) GetByAppIdList(db *gorm.DB, appId int64) []sysmodel.Authority {
	var AppList []sysmodel.Authority
	db.Where(&sysmodel.Authority{AppId: appId}).Find(&AppList)
	return AppList
}

func (r *authorityDao) Create(db *gorm.DB, AppInfo *sysmodel.Authority) (appInfo *sysmodel.Authority, err error) {
	r.IsHasTable(db)
	err = db.Create(AppInfo).Error
	return
}

func (r *authorityDao) IsHasTable(db *gorm.DB) {
	if db.Migrator().HasTable(&sysmodel.Authority{}) == false {
		db.Migrator().CreateTable(&sysmodel.Authority{})
	}
}
