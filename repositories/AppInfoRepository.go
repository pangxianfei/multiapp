package repositories

import (
	"gitee.com/pangxianfei/multiapp/sysmodel"
	"gorm.io/gorm"
)

var AppInfoRepository = new(appInfoRepository)

type appInfoRepository struct {
}

func (r *appInfoRepository) Take(db *gorm.DB, where ...interface{}) *sysmodel.AppInfo {
	ret := &sysmodel.AppInfo{}
	if err := db.Debug().Take(ret, where...).Error; err != nil {
		return nil
	}
	return ret
}
func (r *appInfoRepository) GetByName(db *gorm.DB, name string) *sysmodel.AppInfo {
	return r.Take(db, "name = ?", name)
}

func (r *appInfoRepository) Create(db *gorm.DB, AppInfo *sysmodel.AppInfo) (appInfo *sysmodel.AppInfo, err error) {
	r.IsHasTable(db)
	err = db.Create(AppInfo).Error
	return
}

func (r *appInfoRepository) GetByList(db *gorm.DB) []sysmodel.AppInfo {
	var AppList []sysmodel.AppInfo
	db.Find(&AppList)
	return AppList
}

func (r *appInfoRepository) GetByAppCreateList(db *gorm.DB) []sysmodel.AppInfo {
	var AppList []sysmodel.AppInfo
	db.Where("is_created = ?", 1).Find(&AppList)
	return AppList
}

func (r *appInfoRepository) GetStartApplication(db *gorm.DB) []sysmodel.AppInfo {
	var AppList []sysmodel.AppInfo
	db.Where("status = ?", 1).Find(&AppList)
	return AppList
}

func (r *appInfoRepository) IsHasTable(db *gorm.DB) {
	if db.Migrator().HasTable(&sysmodel.Permissions{}) == false {
		_ = db.Migrator().CreateTable(&sysmodel.Permissions{})
	}
}
