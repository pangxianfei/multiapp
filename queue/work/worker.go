package work

import (
	"github.com/golang/protobuf/proto"

	"gitee.com/pangxianfei/multiapp/kernel/zone"
)

var jobMap map[string]worker

func init() {
	jobMap = make(map[string]worker)
}

func Add(j worker) {
	j.SetParam(j.ParamProto()) // for init
	jobMap[j.Name()] = j
}

type worker interface {
	Name() string

	SetParam(paramPtr proto.Message)
	paramData() proto.Message
	ParamProto() proto.Message

	Handle(paramPtr proto.Message) error

	Retries() uint32

	SetDelay(delay zone.Duration)
	Delay() zone.Duration
}
