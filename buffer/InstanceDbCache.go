package buffer

import (
	"encoding/json"
	"fmt"

	"gitee.com/pangxianfei/multiapp/consts"
	"gitee.com/pangxianfei/multiapp/kernel/cache"
	"gitee.com/pangxianfei/multiapp/simple"
	"gitee.com/pangxianfei/multiapp/sysmodel"
)

var CacheInstanceDb = new(CacheInstance)

type CacheInstance struct {
}

// GetCacheInstanceDb 获取缓存数据
func (c *CacheInstance) GetCacheInstanceDb(AppId int64) (InstanceDb *sysmodel.InstanceDb) {
	// TenantIdDbAppKey := fmt.Sprintf(consts.APP_DB_CACHE_KEY, AppId)
	// TenantIdDbAppKey = simple.MD5(TenantIdDbAppKey)
	// cacheData := cache.GetString(TenantIdDbAppKey)
	// if cacheData == "" {
	// 	return nil
	// }
	// if err := tmaic.InterfaceToStruct(cacheData, &InstanceDb); err != nil {
	// 	return nil
	// }
	// if InstanceDb.ID > 0 && InstanceDb.AppId > 0 {
	// 	return InstanceDb
	// }
	return nil
}

// SetCacheInstanceDb 设置缓存数据
func (c *CacheInstance) SetCacheInstanceDb(instanceDb *sysmodel.InstanceDb) bool {
	if instanceDb == nil || instanceDb.ID <= 0 {
		return false
	}
	instanceDbData, _ := json.Marshal(instanceDb)
	TenantIdDbAppKey := fmt.Sprintf(consts.APP_DB_CACHE_KEY, instanceDb.AppId)
	TenantIdDbAppKey = simple.MD5(TenantIdDbAppKey)
	return cache.AddTokenCache(TenantIdDbAppKey, instanceDbData)
}
