package cmd

type Schedule struct {
	commandList []*ScheduleCommand
}

var scheduleHub *Schedule

func ScheduleCommandList() *[]*ScheduleCommand {
	return &scheduleHub.commandList
}

func NewSchedule() *Schedule {
	var cmdList []*ScheduleCommand
	scheduleHub = &Schedule{commandList: cmdList}
	return scheduleHub
}

func (s *Schedule) Command(commandWithArgData string) *ScheduleCommand {
	name, argDataList := parseArgData(commandWithArgData)

	c, err := getParsedCommand(name)
	if err != nil {
		panic(err)
	}

	if len(argDataList) <= 0 {
		_cmd := newScheduleCommand(c.Handler, newArg(nil))
		s.commandList = append(s.commandList, _cmd)

		return _cmd
	}

	argMap := c.mapArg(argDataList)
	_cmd := newScheduleCommand(c.Handler, newArg(&argMap))
	s.commandList = append(s.commandList, _cmd)
	return _cmd
}
