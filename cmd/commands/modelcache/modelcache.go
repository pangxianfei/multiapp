package modelcache

import (
	"fmt"

	"gitee.com/pangxianfei/multiapp/config"

	"gitee.com/pangxianfei/multiapp/cmd"
)

func init() {
	cmd.Add(&ModelCache{})
}

type ModelCache struct {
}

func (s *ModelCache) Command() string {
	return "modelcache:make {appname} {name}"
}

func (s *ModelCache) Description() string {
	return "Create a AppName Model Cache"
}

func (s *ModelCache) Handler(arg *cmd.Arg) error {
	appname, aErr := arg.Get("appname")
	if aErr != nil {
		return aErr
	}
	modelCacheName, rErr := arg.Get("name")
	if rErr != nil {
		return rErr
	}

	if appname == nil {
		cmd.Warning("You need a app name")
		return nil
	}

	if modelCacheName == nil {
		cmd.Warning("You need a Model Cache name")
		return nil
	}
	var AppPath string = config.GetString("app.app_path")

	var stubsModelCacheNamePath string = fmt.Sprintf("%s/make/stubs/modelcache", config.GetString("app.make_stubs"))

	newAppName := string(*appname)
	newModelCacheName := string(*modelCacheName)

	createModel := cmd.Model{}
	Models := createModel.MakeModelFromString(newAppName, newModelCacheName)
	// 拼接目标文件路径
	filePath := fmt.Sprintf("%s/%s/buffer/%sCache.go", AppPath, Models.AppName, Models.StructName)
	// 基于模板创建文件（做好变量替换）
	createModel.CreateFileFromStub(filePath, stubsModelCacheNamePath, Models)

	return nil
}
