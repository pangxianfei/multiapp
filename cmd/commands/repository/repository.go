package repository

import (
	"fmt"

	"gitee.com/pangxianfei/multiapp/config"

	"gitee.com/pangxianfei/multiapp/cmd"
	"gitee.com/pangxianfei/multiapp/kernel/debug"
)

func init() {
	cmd.Add(&Repository{})
}

type Repository struct {
}

func (s *Repository) Command() string {
	return "Repository:make {appname} {DaoModel}"
}

func (s *Repository) Description() string {
	return "Create a AppName DaoModel"
}

func (s *Repository) Handler(arg *cmd.Arg) error {
	appname, aErr := arg.Get("appname")
	if aErr != nil {
		return aErr
	}
	repositoriesName, rErr := arg.Get("DaoModel")
	if rErr != nil {
		return rErr
	}

	if appname == nil {
		cmd.Warning("You need a app name")
		return nil
	}

	if repositoriesName == nil {
		cmd.Warning("You need a DaoModel name")
		return nil
	}
	var AppPath string = config.GetString("app.app_path")

	var stubsRepositoryMametPath string = fmt.Sprintf("%s/make/stubs/repository", config.GetString("app.make_stubs"))

	newAppName := string(*appname)
	newRepositoriesName := string(*repositoriesName)

	createModel := cmd.Model{}
	Models := createModel.MakeModelFromString(newAppName, newRepositoriesName)

	// 拼接目标文件路径
	filePath := fmt.Sprintf("%s/%s/repositories/%sRepository.go", AppPath, Models.AppName, Models.StructName)
	// 基于模板创建文件（做好变量替换）
	createModel.CreateFileFromStub(filePath, stubsRepositoryMametPath, Models)

	debug.Dd(Models)

	return nil
}
