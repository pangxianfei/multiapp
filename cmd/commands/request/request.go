package request

import (
	"fmt"

	"gitee.com/pangxianfei/multiapp/config"

	"gitee.com/pangxianfei/multiapp/cmd"
)

func init() {
	cmd.Add(&Request{})
}

type Request struct {
}

func (s *Request) Command() string {
	return "request:make {appname} {name}"
}

func (s *Request) Description() string {
	return "Create a request"
}

func (s *Request) Handler(arg *cmd.Arg) error {
	appname, aErr := arg.Get("appname")
	if aErr != nil {
		return aErr
	}
	requestname, rErr := arg.Get("name")
	if rErr != nil {
		return rErr
	}

	if appname == nil {
		cmd.Warning("You need a app name")
		return nil
	}

	if requestname == nil {
		cmd.Warning("You need a request name")
		return nil
	}

	var AppPath string = config.GetString("app.app_path")

	var stubsRequestPath string = fmt.Sprintf("%s/make/stubs/request", config.GetString("app.make_stubs"))

	newAppName := string(*appname)
	newRequestName := string(*requestname)

	createModel := cmd.Model{}
	Models := createModel.MakeModelFromString(newAppName, newRequestName)
	// 拼接目标文件路径
	filePath := fmt.Sprintf("%s/%s/http/request/%sRequest.go", AppPath, Models.AppName, Models.StructName)

	// 基于模板创建文件（做好变量替换）
	createModel.CreateFileFromStub(filePath, stubsRequestPath, Models)

	return nil
}
