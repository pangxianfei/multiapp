package lang

import "gitee.com/pangxianfei/multiapp/context"

type Context interface {
	context.DataContextor
}
