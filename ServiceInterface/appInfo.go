package ServiceInterface

import (
	"gitee.com/pangxianfei/multiapp/requests"
	"gitee.com/pangxianfei/multiapp/sysmodel"
)

type appInfo interface {
	GetByName(mobile string) *sysmodel.AppInfo
	GetByList() []sysmodel.AppInfo
	Create(appInfo requests.AppInfo) (AppInfo *sysmodel.AppInfo, err error)
	GetByAppCreateList() []sysmodel.AppInfo
	GetStartApplication() []sysmodel.AppInfo
}
