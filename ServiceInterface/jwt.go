package ServiceInterface

import "gitee.com/pangxianfei/multiapp/sysmodel"

type jwt interface {
	InitMiddleware(Admin *sysmodel.PlatformAdmin) (string, error)
}
