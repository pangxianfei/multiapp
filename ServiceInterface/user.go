package ServiceInterface

import (
	"gitee.com/pangxianfei/multiapp/sysmodel"
)

type user interface {
	GetByMobile(mobile string) *sysmodel.PlatformAdmin
	GetById(id int64) *sysmodel.PlatformAdmin
	// SignIn(ctx iris.Context, UserLogin requests.UserLogin) (adminInfo *sysmodel.PlatformAdmin, UserTokenSTR string, err error)
	// LoginUsingID(ctx iris.Context, adminId int64) (adminInfo *sysmodel.PlatformAdmin, UserTokenSTR string, err error)
	// UnifyLogin(ctx iris.Context, admin *sysmodel.PlatformAdmin) (UserTokenSTR string, err error)
	// GetUserId(ctx iris.Context) int64
}
