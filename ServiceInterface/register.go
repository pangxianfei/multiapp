package ServiceInterface

import (
	"gitee.com/pangxianfei/multiapp/sysmodel"
	"gorm.io/gorm"
)

type register interface {
	GetByMobile(db *gorm.DB, mobile string) *sysmodel.PlatformAdmin
}
