package ServiceInterface

import (
	"gitee.com/pangxianfei/multiapp/sysmodel"
)

type UserToken interface {
	//GetUserInfo(ctx iris.Context) (adminInfo *sysmodel.PlatformAdmin)
	Create(Admin *sysmodel.PlatformAdmin, token string) (*sysmodel.UserToken, error)
	// Disable(ctx iris.Context) bool
	// Parse(ctx iris.Context) error
}
