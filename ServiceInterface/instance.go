package ServiceInterface

import (
	"gorm.io/gorm"
)

type instance interface {
	GetAppDb(appId int64) *gorm.DB
	//GetAppId(ctx iris.Context) int64
}
