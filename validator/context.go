package validator

import (
	"gitee.com/pangxianfei/multiapp/context"

	"gitee.com/pangxianfei/multiapp/resources/lang"
)

type Context interface {
	context.RequestBindingContextor
	context.ResponseContextor
	lang.Context
}
