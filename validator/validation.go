package validator

import (
	"errors"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	ut "github.com/go-playground/universal-translator"

	"gopkg.in/go-playground/validator.v9"

	"gitee.com/pangxianfei/multiapp/facades"
	"gitee.com/pangxianfei/multiapp/kernel/locale"
	"gitee.com/pangxianfei/multiapp/kernel/trans"
)

type Validation struct {
	Trans *ut.Translator
}

func (v *Validation) Validate(c Context, requestDataPtr interface{}, onlyFirstError bool) error {
	facades.Logger.Warn("Deprecated, for compatible with v0.10.0, you should use ValidateJSON instead.")
	return v.ValidateJSON(c, requestDataPtr, onlyFirstError)
}

func (v *Validation) ValidateJSONMulti(c Context, requestDataPtr interface{}, onlyFirstError bool) error {
	return validate(func(rdp interface{}) error {
		return c.ShouldBindBodyWith(rdp, binding.JSON)
	}, c, requestDataPtr, onlyFirstError)
}
func (v *Validation) ValidateXMLMulti(c Context, requestDataPtr interface{}, onlyFirstError bool) error {
	return validate(func(rdp interface{}) error {
		return c.ShouldBindBodyWith(rdp, binding.XML)
	}, c, requestDataPtr, onlyFirstError)
}
func (v *Validation) ValidateYAMLMulti(c Context, requestDataPtr interface{}, onlyFirstError bool) error {
	return validate(func(rdp interface{}) error {
		return c.ShouldBindBodyWith(rdp, binding.YAML)
	}, c, requestDataPtr, onlyFirstError)
}
func (v *Validation) ValidateMsgPackMulti(c Context, requestDataPtr interface{}, onlyFirstError bool) error {
	return validate(func(rdp interface{}) error {
		return c.ShouldBindBodyWith(rdp, binding.MsgPack)
	}, c, requestDataPtr, onlyFirstError)
}
func (v *Validation) ValidateProtoBufMulti(c Context, requestDataPtr interface{}, onlyFirstError bool) error {
	return validate(func(rdp interface{}) error {
		return c.ShouldBindBodyWith(rdp, binding.ProtoBuf)
	}, c, requestDataPtr, onlyFirstError)
}

func (v *Validation) ValidateJSON(c Context, requestDataPtr interface{}, onlyFirstError bool) error {
	if hasBodyBytesKey(c) {
		return v.ValidateJSONMulti(c, requestDataPtr, onlyFirstError)
	}

	return validate(func(rdp interface{}) error {
		return c.ShouldBindWith(rdp, binding.JSON)
	}, c, requestDataPtr, onlyFirstError)
}
func (v *Validation) ValidateXML(c Context, requestDataPtr interface{}, onlyFirstError bool) error {
	if hasBodyBytesKey(c) {
		return v.ValidateXMLMulti(c, requestDataPtr, onlyFirstError)
	}

	return validate(func(rdp interface{}) error {
		return c.ShouldBindWith(rdp, binding.XML)
	}, c, requestDataPtr, onlyFirstError)
}
func (v *Validation) ValidateYAML(c Context, requestDataPtr interface{}, onlyFirstError bool) error {
	if hasBodyBytesKey(c) {
		return v.ValidateYAMLMulti(c, requestDataPtr, onlyFirstError)
	}

	return validate(func(rdp interface{}) error {
		return c.ShouldBindWith(rdp, binding.YAML)
	}, c, requestDataPtr, onlyFirstError)
}
func (v *Validation) ValidateMsgPack(c Context, requestDataPtr interface{}, onlyFirstError bool) error {
	if hasBodyBytesKey(c) {
		return v.ValidateMsgPackMulti(c, requestDataPtr, onlyFirstError)
	}

	return validate(func(rdp interface{}) error {
		return c.ShouldBindWith(rdp, binding.MsgPack)
	}, c, requestDataPtr, onlyFirstError)
}
func (v *Validation) ValidateProtoBuf(c Context, requestDataPtr interface{}, onlyFirstError bool) error {
	if hasBodyBytesKey(c) {
		return v.ValidateProtoBufMulti(c, requestDataPtr, onlyFirstError)
	}

	return validate(func(rdp interface{}) error {
		return c.ShouldBindWith(rdp, binding.ProtoBuf)
	}, c, requestDataPtr, onlyFirstError)
}
func hasBodyBytesKey(c Context) bool {
	if _, ok := c.Get(gin.BodyBytesKey); ok {
		return true
	}
	return false
}

func (v *Validation) ValidateUri(c Context, requestDataPtr interface{}, onlyFirstError bool) error {
	return validate(c.ShouldBindUri, c, requestDataPtr, onlyFirstError)
}
func (v *Validation) ValidateQuery(c Context, requestDataPtr interface{}, onlyFirstError bool) error {
	return validate(c.ShouldBindQuery, c, requestDataPtr, onlyFirstError)
}
func (v *Validation) ValidateForm(c Context, requestDataPtr interface{}, onlyFirstError bool) error {
	return validate(c.ShouldBind, c, requestDataPtr, onlyFirstError)
}
func (v *Validation) ValidateFormPost(c Context, requestDataPtr interface{}, onlyFirstError bool) error {
	return validate(c.ShouldBind, c, requestDataPtr, onlyFirstError)
}
func (v *Validation) ValidateFormMultipart(c Context, requestDataPtr interface{}, onlyFirstError bool) error {
	return validate(c.ShouldBind, c, requestDataPtr, onlyFirstError)
}

func validate(shouldBindFunc func(rdp interface{}) error, c Context, requestDataPtr interface{}, onlyFirstError bool) error {
	if err := shouldBindFunc(requestDataPtr); err != nil {
		_err, ok := err.(validator.ValidationErrors)
		if !ok {
			return err
		}
		v := binding.Validator.Engine().(*validator.Validate)
		errorResult := trans.ValidationTranslate(v, locale.Locale(c), _err)
		if onlyFirstError {
			return errors.New(errorResult.First())
		}
		return err
	}
	return nil
}
