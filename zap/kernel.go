package zap

import "gitee.com/pangxianfei/multiapp/facades"

func Initialize() {
	facades.Zap = NewZapApplication()
}
