package database

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"time"

	"gitee.com/pangxianfei/multiapp/config"
	"gitee.com/pangxianfei/multiapp/simple"
	"gorm.io/driver/mysql"
	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"

	"gitee.com/pangxianfei/multiapp/database/driver"
	"gitee.com/pangxianfei/multiapp/facades"
	"gitee.com/pangxianfei/multiapp/kernel/zone"
)

func Initialize() {
	// gorm配置
	gormConf := &gorm.Config{}
	// 初始化日志
	InitializationLog(gormConf)
	DbInit(gormConf)
}

// DbInit 平台数据库专用
func DbInit(gormConf *gorm.Config) {
	var UseDbType = config.Instance.DatabaseType.UseDbType
	switch UseDbType {

	case "mysql":
		_ = simple.OpenDB(UseDbType, config.Instance.DB.Dns, config.Instance.DB.Prefix, gormConf, config.Instance.DB.MaxIdleConns, config.Instance.DB.MaxOpenConns)
	case "mssql":
		_ = simple.OpenDB(UseDbType, config.Instance.MSSQLDB.Sqlserver, config.Instance.MSSQLDB.Prefix, gormConf, config.Instance.MSSQLDB.SetMaxIdleConns, config.Instance.MSSQLDB.SetMaxOpenConns)
	default:
		fmt.Printf("出始化数据库失败")
	}
}

//*********************
//特别说明，此包只能使用于租户连接数据库，平台禁用此连接驱动
//租户应用专用
// *******************************************

func OpenDB(DriverName string, Host string, Dbuser string, Password string, Dbname string, Port int, Prefix string, Charset string, SetmaxIdleconns int, Setmaxopenconns int, Setconnmaxlifetime int) (dber databaser, sqlDb *gorm.DB) {

	if DriverName == "" || len(DriverName) == 0 || len(Dbuser) == 0 || Port <= 0 || len(Password) == 0 {
		return nil, nil
	}

	//驱动名称
	switch DriverName {
	//mysql 驱动
	case "mysql":
		dber = driver.NewMysql(DriverName, Host, Dbuser, Password, Dbname, Port, Prefix, Charset)
		gormConf := &gorm.Config{}
		facades.Log.Info("Password:" + Password)
		if gormConf.NamingStrategy == nil {
			gormConf.NamingStrategy = schema.NamingStrategy{
				TablePrefix:   Prefix,
				SingularTable: true,
			}
		}

		InitializationLog(gormConf)
		Db, DbErr := sql.Open("mysql", dber.ConnectionArgs())
		if DbErr != nil {
			facades.Log.Info("failed to connect database by sqlOpen")
			return nil, nil
		}

		var openErr error
		sqlDb, openErr = gorm.Open(mysql.New(mysql.Config{
			DSN:                       dber.ConnectionArgs(),
			DefaultStringSize:         256,   // string 类型字段的默认长度
			DisableDatetimePrecision:  true,  // 禁用 datetime 精度，MySQL 5.6 之前的数据库不支持
			DontSupportRenameIndex:    true,  // 重命名索引时采用删除并新建的方式，MySQL 5.7 之前的数据库和 MariaDB 不支持重命名索引
			DontSupportRenameColumn:   true,  // 用 `change` 重命名列，MySQL 8 之前的数据库和 MariaDB 不支持重命名列
			SkipInitializeWithVersion: false, // 根据版本自动配置
			Conn:                      Db,
		}), gormConf)

		if openErr != nil {
			facades.Log.Info("failed to connect database by DbOpen")
			return nil, nil
		}

		PingErr := Db.Ping()
		if PingErr != nil {
			facades.Log.Info("failed to connect database by ping")
			return nil, nil
		}

		Db.SetConnMaxLifetime(time.Hour)
		Db.SetMaxIdleConns(SetmaxIdleconns)
		Db.SetMaxOpenConns(Setmaxopenconns)
		Db.SetConnMaxLifetime(zone.Duration(Setconnmaxlifetime) * zone.Second)
		return dber, sqlDb
	//mssql 驱动
	case "mssql":
		dber = driver.NewMssql(DriverName, Host, Dbuser, Password, Dbname, Port, Prefix)
		gormConf := &gorm.Config{}
		InitializationLog(gormConf)
		if gormConf.NamingStrategy == nil {
			gormConf.NamingStrategy = schema.NamingStrategy{
				TablePrefix:   Prefix,
				SingularTable: true,
			}
		}
		sqlDb, _ := gorm.Open(sqlserver.Open(dber.ConnectionArgs()), gormConf)
		db, sqlDbErr := sqlDb.DB()
		if sqlDbErr == nil {
			db.SetMaxIdleConns(SetmaxIdleconns)
			db.SetMaxOpenConns(Setmaxopenconns)
			db.SetConnMaxLifetime(time.Hour)
			db.SetConnMaxLifetime(zone.Duration(Setconnmaxlifetime) * zone.Second)
			return dber, sqlDb
		} else {
			facades.Log.Info(sqlDbErr.Error())
		}
	default:
	}
	return nil, nil
}

func InitializationLog(gormConf *gorm.Config) bool {

	if file, err := os.OpenFile(config.GetString("app.Log_file"), os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666); err == nil {
		if config.GetBool("database.show-sql") {
			gormConf.Logger = logger.New(log.New(file, "\r\n", log.LstdFlags), logger.Config{
				SlowThreshold: time.Second,
				Colorful:      true,
				LogLevel:      logger.Info,
			})
		}
	} else {
		facades.Log.Info(err.Error())
		return false
	}
	return true
}
