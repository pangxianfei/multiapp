package driver

import (
	"fmt"
)

type mssql struct {
	ConnDns     string
	DbDriver    string
	Host        string
	UserName    string
	Password    string
	Database    string
	Port        int
	TablePrefix string
	Collation   string
	Charset     string
}

func NewMssql(DriverName string, Host string, Dbuser string, Password string, Database string, Port int, Prefix string) *mssql {
	_db := new(mssql)
	_db.setConnection(DriverName, Host, Dbuser, Password, Database, Port, Prefix)
	return _db
}
func (mssq *mssql) Driver() string {
	return mssq.DbDriver
}

func (mssq *mssql) Prefix() string {
	return mssq.TablePrefix
}

func (mssq *mssql) setConnection(DriverName string, Host string, UserName string, Password string, Database string, Port int, Prefix string) {
	mssq.DbDriver = DriverName
	mssq.Host = Host
	mssq.UserName = UserName
	mssq.Password = Password
	mssq.Database = Database
	mssq.Port = Port
	mssq.TablePrefix = Prefix
	mssq.ConnDns = fmt.Sprintf("server=%s;user id=%s;password=%s;port=%d;database=%s;encrypt=disable",
		mssq.GetHost(),
		mssq.GetUsername(),
		mssq.GetPassword(),
		mssq.GetPort(),
		mssq.GetDatabase(),
	)
}

func (mssq *mssql) connection() string {
	return mssq.ConnDns
}

func (mssq *mssql) GetUsername() string {
	return mssq.UserName
}
func (mssq *mssql) GetPassword() string {
	return mssq.Password
}
func (mssq *mssql) GetHost() string {
	return mssq.Host
}
func (mssq *mssql) GetPort() int {
	return mssq.Port
}
func (mssq *mssql) GetDatabase() string {
	return mssq.Database
}
func (mssq *mssql) GetCharset() string {
	return mssq.Charset
}
func (mssq *mssql) GetPrefix() string {
	return mssq.TablePrefix
}

func (mssq *mssql) GetDriver() string {
	return mssq.DbDriver
}

func (mssq *mssql) GetCollation() string {
	return mssq.Collation
}
func (mssq *mssql) ConnectionArgs() string {
	return mssq.ConnDns
}
