package policy

import (
	"gitee.com/pangxianfei/multiapp/context"
	"gitee.com/pangxianfei/multiapp/request/http/auth"
)

type Context interface {
	context.LifeCycleContextor
	context.ResponseContextor
	auth.Context
	auth.RequestIUser
}
