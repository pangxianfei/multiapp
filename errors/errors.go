package errors

import (
	"fmt"

	"github.com/ztrue/tracerr"

	"gitee.com/pangxianfei/multiapp/facades"
	"gitee.com/pangxianfei/multiapp/tmaic"
)

func ErrPrintln(err error, fields tmaic.V) {
	startFrom := 2
	if err == nil {
		return
	}
	traceErr := tracerr.Wrap(err)
	frameList := tracerr.StackTrace(traceErr)
	if startFrom > len(frameList) || len(frameList)-2 <= 0 {
		//log.Println(logs.ERROR, err.Error(), fields)
		facades.Log.Errorf(err.Error(), fields)
	}

	traceErr = tracerr.CustomError(err, frameList[startFrom:len(frameList)-2])
	traceErr = tracerr.CustomError(err, frameList)

	if fields == nil {
		fields = tmaic.V{}
	}
	fields["tmaic_trace"] = tracerr.SprintSource(traceErr, 0)
	facades.Log.Errorf(err.Error(), fields)
}

func ErrPrint(err error, startFrom int, fields tmaic.V) string {
	if err == nil {
		return ""
	}
	traceErr := tracerr.Wrap(err)
	frameList := tracerr.StackTrace(traceErr)

	traceErr = tracerr.CustomError(err, frameList)

	if fields == nil {
		fields = tmaic.V{}
	}
	fields["tmaic_trace"] = tracerr.SprintSource(traceErr)
	return fmt.Sprint(err.Error(), fields)
}
