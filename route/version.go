package route

import (
	"gitee.com/pangxianfei/multiapp/config"
	"github.com/gin-gonic/gin"

	"gitee.com/pangxianfei/multiapp/http/middleware"

	"gitee.com/pangxianfei/multiapp/request"
)

type version struct {
	engine *request.Engine
	group  *gin.RouterGroup
	prefix string
}

func NewVersion(engine *request.Engine, prefix string) *version {
	ver := &version{engine: engine, prefix: prefix}
	ver.group = ver.engine.Group(prefix)
	return ver
}

func (v *version) Auth(relativePath string, groupFunc func(grp Grouper), handlers ...request.HandlerFunc) {
	signKey := config.GetString("auth.sign_key")
	ginGroup := v.group.Group(relativePath, request.ConvertHandlers(append([]request.HandlerFunc{middleware.AuthRequired(signKey)}, handlers...))...)
	groupFunc(&group{engineHash: v.engine.Hash(), RouterGroup: ginGroup})
}

func (v *version) NoAuth(relativePath string, groupFunc func(grp Grouper), handlers ...request.HandlerFunc) {
	ginGroup := v.group.Group(relativePath, request.ConvertHandlers(handlers)...)
	groupFunc(&group{engineHash: v.engine.Hash(), RouterGroup: ginGroup})
}
