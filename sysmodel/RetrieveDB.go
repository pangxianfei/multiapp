package sysmodel

// RetrieveDB 检索租户对象
type RetrieveDB struct {
	AppId   int64 // 应用id
	UseType string
	Status  int64  // 状态
	Code    string // 状态
}
