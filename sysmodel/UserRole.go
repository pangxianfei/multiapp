package sysmodel

type UserRoles struct {
	Id     int64 `gorm:"primaryKey;autoIncrement" json:"id"`
	RoleId int64 `gorm:"column:role_id;type:int(11) UNSIGNED" json:"roleId"`
	UserId int64 `gorm:"column:user_id;type:int(11)" json:"userId"`
	AppId  int64 `gorm:"column:app_id;type:int(11) UNSIGNED" json:"app_id"`
}

func (u *UserRoles) TableName() string {
	return "sys_user_has_roles"
}
