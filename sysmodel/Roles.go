package sysmodel

import (
	"gitee.com/pangxianfei/multiapp/kernel/zone"
)

// SysRoles 角色表
type SysRoles struct {
	Id          int64             `gorm:"column:id;type:int(11) unsigned;primary_key;AUTO_INCREMENT" json:"id"`
	Name        string            `gorm:"column:name;type:varchar(255);NOT NULL" json:"name"`
	AppId       int64             `gorm:"column:app_id;type:int(11) unsigned;NOT NULL" json:"app_id"`
	CreatedAt   zone.Time         `gorm:"column:created_at"`
	UpdatedAt   zone.Time         `gorm:"column:updated_at"`
	Permissions []RolePermissions `gorm:"foreignKey:RoleId"`
}

// TableName 指定表
func (s *SysRoles) TableName() string {
	return "sys_roles"
}
