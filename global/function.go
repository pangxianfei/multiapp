package global

import (
	"database/sql"
	"fmt"

	"gitee.com/pangxianfei/multiapp/consts"
	"gitee.com/pangxianfei/multiapp/database"
	"gitee.com/pangxianfei/multiapp/simple"
	"gitee.com/pangxianfei/multiapp/sysmodel"
	"gorm.io/gorm"

	"gitee.com/pangxianfei/multiapp/buffer"
)

func SetAppExampleDB(AppId int64) (AppDB *gorm.DB) {
	if AppId <= 0 {
		return nil
	}
	var CurrInstanceDB *sysmodel.InstanceDb

	var AppExampleDBDBNo string
	AppExampleDBDBNo = fmt.Sprintf(consts.APP_DB_KEY, AppId)
	newAppIdAppDB := buffer.CacheInstanceDb.GetCacheInstanceDb(AppId)
	if newAppIdAppDB != nil && newAppIdAppDB.ID > 0 && newAppIdAppDB.AppId == AppId {
		CurrInstanceDB = newAppIdAppDB
	} else {
		tenantDbWhere := &sysmodel.RetrieveDB{AppId: AppId}
		if err := simple.DB().Debug().Where(tenantDbWhere).Find(&CurrInstanceDB).Error; err != nil {
			return nil
		}
		buffer.CacheInstanceDb.SetCacheInstanceDb(CurrInstanceDB)
	}

	if _, ok := InstanceDBMapsObject[AppExampleDBDBNo]; ok && InstanceDBMapsObject[AppExampleDBDBNo].Db != nil {
		var ThemTenantsDB *sysmodel.InstanceObjectDB
		ThemTenantsDB = InstanceDBMapsObject[AppExampleDBDBNo]
		if ThemTenantsDB.AppId == AppId && CurrInstanceDB.AppId == AppId {
			AppDB = InstanceDBMapsObject[AppExampleDBDBNo].Db
		}
	}
	if AppDB != nil {
		return AppDB.Session(&gorm.Session{})
	}
	return SetSourceData(CurrInstanceDB)
}

func SetSourceData(CurrInstanceDB *sysmodel.InstanceDb) (InstanceDB *gorm.DB) {

	var mysqlStr string
	var dber *sql.DB
	AppExampleDBDBNo := fmt.Sprintf(consts.APP_DB_KEY, CurrInstanceDB.AppId)

	InstanceDB = MysqlOpenDb(CurrInstanceDB)

	SaveConnectionObject := sysmodel.InstanceObjectDB{}
	SaveConnectionObject.DbName = CurrInstanceDB.DbName
	SaveConnectionObject.AppName = CurrInstanceDB.AppName
	SaveConnectionObject.AppId = CurrInstanceDB.AppId
	SaveConnectionObject.Prefix = CurrInstanceDB.Prefix
	SaveConnectionObject.DriverName = CurrInstanceDB.DriverName
	SaveConnectionObject.Db = InstanceDB
	SaveConnectionObject.Dber = dber
	SaveConnectionObject.ConnStr = mysqlStr
	InstanceDBMutex.Lock()
	defer InstanceDBMutex.Unlock()
	InstanceDBMapsObject[AppExampleDBDBNo] = &SaveConnectionObject
	return InstanceDB
}

func MysqlOpenDb(Instance *sysmodel.InstanceDb) (AppDB *gorm.DB) {
	var DbDns string
	//如果是使用外网IP
	if Instance.NetSwitch == 2 {
		DbDns = Instance.OuterNet
	} else {
		//内网IP
		DbDns = Instance.Host
	}
	_, AppDB = database.OpenDB(Instance.DriverName, DbDns, Instance.Dbuser, Instance.Password, Instance.DbName, Instance.Port, Instance.Prefix, Instance.Charset, Instance.SetmaxIdleconns, Instance.Setmaxopenconns, Instance.Setconnmaxlifetime)
	return AppDB
}
