package global

import (
	"sync"

	"gitee.com/pangxianfei/multiapp/sysmodel"
)

var (
	InstanceDBMutex      sync.Mutex
	InstanceDBMapsObject = make(map[string]*sysmodel.InstanceObjectDB)
)
