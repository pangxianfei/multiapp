package application

import (
	"gitee.com/pangxianfei/multiapp/sysmodel"
)

type Application struct {
}

func (app *Application) GetById(id int64) *sysmodel.PlatformAdmin {
	return nil
}

func (app *Application) GetByMobile(mobile string) *sysmodel.PlatformAdmin {
	return nil
}

// GetByName 根据名称查询
func (app *Application) GetByName(Name string) *sysmodel.PlatformAdmin {
	return nil
}

// GetAdminInfo 登陆帐号信息
func (app *Application) GetAdminInfo(id int64) *sysmodel.PlatformAdmin {
	return nil
	//return services.UserService.GetById(id)
}
