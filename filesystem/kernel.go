package filesystem

import "gitee.com/pangxianfei/multiapp/facades"

func Initialize() {
	facades.Storage = NewStorage()
}
