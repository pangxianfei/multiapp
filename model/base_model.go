package model

import (
	"errors"
	"fmt"
	"reflect"
	"sync"

	"github.com/iancoleman/strcase"
	//"gitee.com/pangxianfei/multiapp/simple"
	"gorm.io/gorm"
)

type BaseModel struct{}

func (bm *BaseModel) SetTableName(tableName string) string {
	return fmt.Sprintf("%s", tableName)
}

func (bm *BaseModel) BeforeSave(scope *gorm.DB) (err error) {
	defer func() {
		if bsErr := recover(); bsErr != nil {
			if newErr, ok := bsErr.(error); ok {
				err = newErr
				return
			}
			err = errors.New(fmt.Sprint(bsErr))
			return
		}
	}()
	callMutator(scope, false)
	return nil
}
func (bm *BaseModel) BeforeCreate(scope *gorm.DB) (err error) {
	defer func() {
		if bcErr := recover(); bcErr != nil {
			if newErr, ok := bcErr.(error); ok {
				err = newErr
				return
			}
			err = errors.New(fmt.Sprint(bcErr))
			return
		}
	}()
	callMutator(scope, false)
	return nil
}

func (bm *BaseModel) BeforeUpdate(scope *gorm.DB) (err error) {
	defer func() {
		if beforeErr := recover(); beforeErr != nil {
			if newErr, ok := beforeErr.(error); ok {
				err = newErr
				return
			}
			err = errors.New(fmt.Sprint(beforeErr))
			return
		}
	}()
	callMutator(scope, false)
	return nil
}

func (bm *BaseModel) AfterFind(scope *gorm.DB) (err error) {
	defer func() {
		if findErr := recover(); findErr != nil {
			if newErr, ok := findErr.(error); ok {
				err = newErr
				return
			}
			err = errors.New(fmt.Sprint(findErr))
			return
		}
	}()
	callMutator(scope, true)
	return nil
}

func callMutator(scope *gorm.DB, isGetter bool) {

	var reflectValue reflect.Value
	if reflectValue.CanAddr() && scope.Statement.ReflectValue.Kind() != reflect.Ptr {
		reflectValue = scope.Statement.ReflectValue
	}

	for i := 0; i < len(scope.Statement.Schema.Fields); i++ {
		switch scope.Statement.ReflectValue.Kind() {
		case reflect.Struct:
			structReflect(&scope.Statement.ReflectValue, isGetter)
		case reflect.Slice:
			for i := 0; i < reflectValue.Elem().Len(); i++ {
				rv := reflectValue.Elem().Index(i)
				structReflect(&rv, isGetter)
			}
		default:
			panic("cannot use mutator in type:" + reflectValue.Type().Elem().Kind().String())
		}
	}
}

func structReflect(reflectValue *reflect.Value, isGetter bool) {
	wg := &sync.WaitGroup{}
	if reflectValue.CanAddr() && reflectValue.Kind() != reflect.Ptr {
		tmp := reflectValue.Addr()
		reflectValue = &tmp
	}
	for i := 0; i < reflectValue.Type().Elem().NumField(); i++ {
		wg.Add(1)
		go func(wg *sync.WaitGroup, index int) {
			fieldName := reflectValue.Type().Elem().Field(index).Name
			fieldValue := reflectValue.Elem().Field(index).Interface()
			if isGetter {
				getter(reflectValue, fieldName, fieldValue)
			} else {
				setter(*reflectValue, fieldName, fieldValue)
			}
			wg.Done()
		}(wg, i)
	}
	wg.Wait()
}
func setter(reflectValue reflect.Value, fieldName string, fieldValue interface{}) {
	const setterMethodTemplate = "Set%sAttribute"
	methodName := fmt.Sprintf(setterMethodTemplate, strcase.ToCamel(fieldName))
	if methodValue := reflectValue.MethodByName(methodName); methodValue.IsValid() {
		methodValue.Interface().(func(value interface{}))(fieldValue)
	}
}
func getter(reflectValue *reflect.Value, fieldName string, fieldValue interface{}) {
	const getterMethodTemplate = "Get%sAttribute"
	methodName := fmt.Sprintf(getterMethodTemplate, strcase.ToCamel(fieldName))
	if methodValue := reflectValue.MethodByName(methodName); methodValue.IsValid() {
		newGetData := methodValue.Interface().(func(value interface{}) interface{})(fieldValue)
		reflectValue.Elem().FieldByName(fieldName).Set(reflect.ValueOf(newGetData))
	}
}
