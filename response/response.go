package response

import (
	"net/http"

	"gitee.com/pangxianfei/multiapp/request"
	"gitee.com/pangxianfei/multiapp/tmaic"
)

type JsonResult struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
	Success bool        `json:"success"`
	Err     error
}

const (
	FAIL = 1
	OK   = 0
)

func Fail() *JsonResult {
	return Json(FAIL, "操作失败", map[string]interface{}{}, false)
}
func Json(code int, message string, data interface{}, success bool) *JsonResult {
	return &JsonResult{
		Code:    code,
		Message: message,
		Data:    data,
		Success: success,
	}
}

// JSON gin 框架使用
func JSON(ctx request.Context, resp *JsonResult) {
	// 响应客户端
	ctx.JSON(http.StatusOK, tmaic.V{
		"code":    resp.Code,
		"Message": resp.Message,
		"data":    resp.Data,
		"Success": true,
	})
}

func JsonData(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    200,
		Data:    data,
		Success: true,
	}
}

// JsonCreateSucces 创建成功返回专用
func JsonCreateSucces(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    200,
		Message: "创建成功",
		Data:    data,
		Success: true,
	}
}

// JsonCreateFail 创建失败返回专用
func JsonCreateFail(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    201,
		Message: "创建失败,请稍后尝试~",
		Data:    data,
		Success: false,
	}
}

// JsonQueryData 查询返回专用
func JsonQueryData(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    200,
		Message: "查询成功",
		Data:    data,
		Success: true,
	}
}

// JsonUpdateSuccess 更新成功返回专用
func JsonUpdateSuccess(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    200,
		Message: "更新成功",
		Data:    data,
		Success: true,
	}
}

func JsonUpdateFail(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    200,
		Message: "更新失败,请稍后尝试~",
		Data:    data,
		Success: true,
	}
}

func JsonDeleteSuccess(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    200,
		Message: "删除成功",
		Data:    data,
		Success: true,
	}
}

func JsonDeleteFail(data string) *JsonResult {
	return &JsonResult{
		Code:    201,
		Message: "删除失败,请稍后尝试~",
		Data:    data,
		Success: true,
	}
}

func JsonItemList(data []interface{}) *JsonResult {
	return &JsonResult{
		Code:    200,
		Data:    data,
		Success: true,
	}
}

func JsonSuccess() *JsonResult {
	return &JsonResult{
		Code:    200,
		Message: "请求成功",
		Data:    nil,
		Success: true,
	}
}

func JsonErrorMsg(message string) *JsonResult {
	return &JsonResult{
		Code:    201,
		Message: message,
		Data:    nil,
		Success: false,
	}
}

func JsonError(err error) *JsonResult {
	return &JsonResult{
		Code:    201,
		Message: err.Error(),
		Data:    nil,
		Success: false,
	}
}

// JsonFail 请求失败专用
func JsonFail(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    201,
		Message: "请求失败,请稍后尝试~",
		Data:    data,
		Success: false,
	}
}

func JsonErrorCode(code int, message string) *JsonResult {
	return &JsonResult{
		Code:    code,
		Message: message,
		Data:    nil,
		Success: false,
	}
}

func JsonErrorData(code int, message string, data interface{}) *JsonResult {
	return &JsonResult{
		Code:    code,
		Message: message,
		Data:    data,
		Success: false,
	}
}

func JsonDataError(data interface{}) *JsonResult {
	return &JsonResult{
		Code:    401,
		Message: "提交失败,请稍后尝试~",
		Data:    data,
		Success: false,
	}
}

type RspBuilder struct {
	Data map[string]interface{}
}

func (builder *RspBuilder) Put(key string, value interface{}) *RspBuilder {
	builder.Data[key] = value
	return builder
}

func (builder *RspBuilder) Build() map[string]interface{} {
	return builder.Data
}

func (builder *RspBuilder) JsonResult() *JsonResult {
	return JsonData(builder.Data)
}

////////////////////
///
///
////////////

// ErrorModel 错误返回模型
type ErrorModel struct {
	Code    int64       `json:"code"`
	Message string      `json:"message"`
	Success bool        `json:"success"`
	Data    interface{} `json:"Data"`
}

// ErrorUnregisteredTenantAppDb 403-未没注册的应用
func ErrorUnregisteredTenantAppDb() ErrorModel {
	return ErrorModel{
		Code:    403,
		Message: "未注册的应用或者数据库连接失败",
		Success: false,
		Data:    "未注册的应用或者数据库连接失败",
	}
}

func Error(data interface{}) ErrorModel {
	return ErrorModel{
		Code:    403,
		Message: "没有权限",
		Success: false,
		Data:    data,
	}
}
func ErrorNoHaveAuthority() ErrorModel {
	return ErrorModel{
		Code:    403,
		Message: "没有权限",
		Success: false,
		Data:    "没有权限",
	}
}

// ErrorTokenInvalidation 401-未认证登录
func ErrorTokenInvalidation() ErrorModel {
	return ErrorModel{
		Code:    401,
		Message: "令牌失效",
		Success: false,
		Data:    "令牌失效",
	}
}

// ErrorUnauthorized 401-未认证登录
func ErrorUnauthorized() ErrorModel {
	return ErrorModel{
		Code:    401,
		Message: "未认证登录",
		Success: false,
		Data:    "未认证登录",
	}
}
