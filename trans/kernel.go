package trans

import (
	"gitee.com/pangxianfei/multiapp/config"
	"gitee.com/pangxianfei/multiapp/resources/lang"
	"gitee.com/pangxianfei/multiapp/resources/lang/helper"
	"gopkg.in/go-playground/validator.v9"
)

func ValidationTranslate(v *validator.Validate, langName string, e validator.ValidationErrors) lang.ValidationError {
	return helper.ValidationTranslate(v, langName, e)
}
func CustomTranslate(messageID string, data map[string]interface{}, langName string) string {
	return helper.CustomTranslate(messageID, data, langName)
}

func Get(messageID string, dataLocal ...interface{}) string {

	data := make(map[string]interface{})
	switch len(dataLocal) {
	case 1:
		data = dataLocal[0].(map[string]interface{})
		break
	default:
	}

	return CustomTranslate(messageID, data, config.Instance.AppLocale)
}
